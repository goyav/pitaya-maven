<html>
<head>
  <title>Book Query</title>
</head>
<body>
  <h1>Another E-Bookstore</h1>
  <h3>Choose Author(s):</h3>
  <form method="get">
    <input type="checkbox" name="author" value="1313bab9-ab4a-4810-a340-ff0f72e6a2cb">Tan
    <input type="checkbox" name="author" value="1e708363-070e-4ed1-bf61-7f11e9dde091">Ali
    <input type="checkbox" name="author" value="Kumar">Kumar
    <input type="submit" value="Query">
  </form>
 
  <%
    String[] authors = request.getParameterValues("author");
    if (authors != null) {
  %>
  <%@ page import = "java.sql.*" %>
  <%
      Class.forName("org.postgresql.Driver");
      Connection conn = DriverManager.getConnection("jdbc:postgresql://192.168.0.11:5432/pitaya?user=suntzu974&password=HpNKUsNN27031968");
      Statement stmt = conn.createStatement();
 
      String sqlStr = "SELECT title,description,author FROM articles  WHERE author IN (";
      sqlStr += "'" + authors[0] + "'";  // First author
      for (int i = 1; i < authors.length; ++i) {
         sqlStr += ", '" + authors[i] + "'";  // Subsequent authors need a leading commas
      }
      sqlStr += ") ORDER BY author ASC, title ASC";
 
      // for debugging
      System.out.println("Query statement is " + sqlStr);
      ResultSet rset = stmt.executeQuery(sqlStr);
  %>
      <hr>
      <form method="get" action="order.jsp">
        <table border=1 cellpadding=5>
          <tr>
            <th>Title</th>
            <th>Description</th>
          </tr>
  <%
      while (rset.next()) {
        String author = rset.getString("author");
  %>
          <tr>
            <td><input type="checkbox" name="author" value="<%= author %>"></td>
            <td><%= rset.getString("title") %></td>
            <td><%= rset.getString("description") %></td>
          </tr>
  <%
      }
  %>
        </table>
        <br>
        <input type="submit" value="Order">
        <input type="reset" value="Clear">
      </form>
      <a href="<%= request.getRequestURI() %>"><h3>Back</h3></a>
  <%
      rset.close();
      stmt.close();
      conn.close();
    }
  %>
</body>
</html>